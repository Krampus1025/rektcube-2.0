#include <cmath>

class Vec3D{
public:
	Vec3D() : x(0), y(0), z(0) { }
	Vec3D(float x, float y, float z) : x(x), y(y), z(z) { }

	float x, y, z;
	float magnitude() { return sqrt((x * x) + (y * y) + (z * z)); }
	float dotProduct(const Vec3D& vector) { return ((x * vector.x) + (y * vector.y) + (z * vector.z)); }
	void normalize(){ float mag = magnitude(); *this = Vec3D(x / mag, y / mag, z / mag); }

	inline Vec3D operator-(const Vec3D& vector) {
		Vec3D newVec;
		newVec.x = x - vector.x;
		newVec.y = y - vector.y;
		newVec.z = z - vector.z;
		return newVec;
	}

	inline Vec3D operator-(const float& rhs) {
		Vec3D newVec;
		newVec.x = x - rhs;
		newVec.y = y - rhs;
		newVec.z = z - rhs;
		return newVec;
	}

	inline Vec3D operator+(const Vec3D& vector) {
		Vec3D newVec;
		newVec.x = x + vector.x;
		newVec.y = y + vector.y;
		newVec.z = z + vector.z;
		return newVec;
	}

	inline Vec3D operator+(const float& rhs) {
		Vec3D newVec;
		newVec.x = x + rhs;
		newVec.y = y + rhs;
		newVec.z = z + rhs;
		return newVec;
	}

	inline Vec3D operator/(const Vec3D& vector){
		Vec3D newVec;
		newVec.x = x / vector.x;
		newVec.y = y / vector.y;
		newVec.z = z / vector.z;
		return newVec;
	}

	inline Vec3D operator/(const float& rhs){
		Vec3D newVec;
		newVec.x = x / rhs;
		newVec.y = y / rhs;
		newVec.z = z / rhs;
		return newVec;
	}

	inline Vec3D operator*(const Vec3D& vector){
		Vec3D newVec;
		newVec.x = x * vector.x;
		newVec.y = y * vector.y;
		newVec.z = z * vector.z;
		return newVec;
	}

	inline Vec3D operator*(const float& rhs){
		Vec3D newVec;
		newVec.x = x * rhs;
		newVec.y = y * rhs;
		newVec.z = z * rhs;
		return newVec;
	}

	inline void operator=(const Vec3D& vector){
		x = vector.x;
		y = vector.y;
		z = vector.z;
	}

	inline void operator-=(const Vec3D& vector){
		x = x - vector.x;
		y = y - vector.y;
		z = z - vector.z;
	}

	inline void operator-=(const float& rhs){
		x = x - rhs;
		y = y - rhs;
		z = z - rhs;
	}

	inline void operator+=(const Vec3D& vector){
		x = x + vector.x;
		y = y + vector.y;
		z = z + vector.z;
	}

	inline void operator+=(const float& rhs){
		x = x + rhs;
		y = y + rhs;
		z = z + rhs;
	}

	inline void operator/=(const Vec3D& vector){
		x = x / vector.x;
		y = y / vector.y;
		z = z / vector.z;
	}

	inline void operator/=(const float& rhs){
		x = x / rhs;
		y = y / rhs;
		z = z / rhs;
	}

	inline void operator*=(const Vec3D& vector){
		x = x * vector.x;
		y = y * vector.y;
		z = z * vector.z;
	}

	inline void operator*=(const float& rhs){
		x = x * rhs;
		y = y * rhs;
		z = z * rhs;
	}
};
#include "Functions.h"

#define out(c) _cprintf(c)

enum glHkFuncts{
	GLBEG,
	GLCLR,
	GLSWAPBUFFS
};

void PlaceHooks(trpHook * hkArr){
	// Make sure all hooks are placed before running code
	out("Hooking wglSwapBuffers...\n");
	owglSwapBuffers = (twglSwapBuffers)hkArr[GLSWAPBUFFS].ToggleHook(owglSwapBuffers);
	if (owglSwapBuffers != nullptr)
		out("wglSwapBuffers Hooked Successfully!\n\n");
	else
		out("Failed to hook wglSwapBuffers!\n\n");
	

	out("Hooking glBegin...\n");
	oglBegin = (tglBegin)hkArr[GLBEG].ToggleHook(oglBegin);
	if (oglBegin != nullptr)
		out("glBegin Hooked Successfully!\n\n");
	else
		out("Failed to hook glBegin!\n\n");


	out("Hooking glClear...\n");
	oglClear = (tglClear)hkArr[GLCLR].ToggleHook(oglClear);
	if (oglClear != nullptr)
		out("glClear Hooked Successfully!\n\n");
	else
		out("Failed to hook glClear!\n\n");
}

void HandleToggles(){
	if (GetAsyncKeyState(VK_NUMPAD1) & 1) radar->Toggle();
	if (GetAsyncKeyState(VK_NUMPAD2) & 1) esp->Toggle();
	if (GetAsyncKeyState(VK_NUMPAD3) & 1) gEsp->Toggle();
	if(GetAsyncKeyState(VK_NUMPAD4) & 1) Menu = !Menu;
	if (GetAsyncKeyState(VK_NUMPAD5) & 1) Wallhack = !Wallhack;

	// Flip the aimbot type
	if (GetAsyncKeyState(VK_NUMPAD6) & 1){
		int curSmooth = aim->getSmoothing();

		switch (aim->curType()){
		case 0:
			delete aim;
			aim = new nearCross(curSmooth);
			break;

		case 1:
			delete aim;
			aim = new nearPlayer(curSmooth);
			break;
		}
	}

	if (GetAsyncKeyState(VK_RBUTTON)) aim->activate();

	// Adjustments for aimbot smoothness
	if (GetAsyncKeyState(VK_ADD) & 1) aim->setSmoothing(aim->getSmoothing() + 100);
	if (GetAsyncKeyState(VK_SUBTRACT) & 1 && aim->getSmoothing() > 0) aim->setSmoothing(aim->getSmoothing() - 100);

	// Adjustments for the radar
	if (radar->State()){
		if (GetAsyncKeyState(VK_UP) & 1)
			radar->Grow();

		if (GetAsyncKeyState(VK_DOWN) & 1)
			radar->Shrink();

		if (GetAsyncKeyState(VK_LEFT) & 1)
			radar->ZoomOut();

		if (GetAsyncKeyState(VK_RIGHT) & 1)
			radar->ZoomIn();
	}
}

DWORD WINAPI Engine(LPVOID param){
	// Place all of the needed hooks
	trpHook * hkArr = new trpHook[3] {	
		trpHook(GetProcAddress(hMod, "glBegin"), hkglBegin, 6), 
		trpHook(GetProcAddress(hMod, "glClear"), hkglClear, 7), 
		trpHook(GetProcAddress(hMod, "wglSwapBuffers"), hkwglSwapBuffs, 5) 
	};

	aim = new nearPlayer;
	radar = new Radar;
	esp = new ESP;
	gEsp = new GrenadeESP;

	PlaceHooks(hkArr);
	out("\n\nHappy Hacking!\n\n");

	while (true){
		HandleToggles();

		//Freeing the DLL
		if (GetAsyncKeyState(VK_NUMPAD9) & 1){
			// Unload the free the hooks
			for (int i = 0; i < 3; i++) hkArr[i].Unload();
			delete[] hkArr;

			// Free the hack memory
			delete aim;
			delete radar;
			delete esp;
			delete gEsp;

			// Free the console and the DLL
			FreeConsole();
			FreeLibraryAndExitThread((HMODULE)param, NULL);
		}
	}

	return NULL;
}

//Our entry point
BOOL WINAPI DllMain(HINSTANCE instance, DWORD reason, LPVOID reserved){
	switch (reason) {
	case DLL_PROCESS_ATTACH:
		CreateThread(0, 0, Engine, instance, 0, 0);
		AllocConsole();
		AttachConsole(GetProcessId(instance));
		break;
	case DLL_THREAD_ATTACH:
	case DLL_PROCESS_DETACH:
	case DLL_THREAD_DETACH:
		break;
	}

	return TRUE;
}
#include <cube.h>
#include <gl/glut.h>
#include "Graphics.h"
#include "ESP.h"

void ESP::Draw(){
	typedef playerent * (_fastcall *_GetClientById)(int cn);
	_GetClientById GetClientById = _GetClientById((DWORD)GetModuleHandle(NULL) + 0x27320);

	playerent * pLocal = *(playerent**)(0x50F4F4);

	for (int i = 0; i < 32; i++){
		GLfloat tmpColors[3] = { 0.0f, 0.0f, 0.0f };

		// Get the entity in the index
		playerent * ent = (playerent*)GetClientById(i);

		// Make sure it's a valid entity
		if (!ent || ent == pLocal || ent->state == CS_DEAD)
			continue;

		// Assign the vMatrix
		glmatrixf * glmvpmatrix = (glmatrixf*)(0x501AE8);

		// Store the entity position in a vec object
		vec pos(ent->o.x, ent->o.y, ent->o.z);

		// Check if they're off the screen
		if (glmvpmatrix->transformw(pos) < 0.0f)
			continue;

		else{
			glPushMatrix();

			// Width and height of the game window
			int width = *(DWORD*)0x510C94;
			int height = *(DWORD*)0x510C98;

			// Make sure we're drawing in an area of 800x600 (Or whatever resolution the player has)
			glViewport(0, 0, width, height);
			glMatrixMode(GL_PROJECTION);
			glLoadIdentity();
			glOrtho(0, width, height, 0, -1, 1);
			glMatrixMode(GL_MODELVIEW);
			glLoadIdentity();

			// Transform the position to screen coords
			vec transformedPos = glmvpmatrix->Transform(glmvpmatrix, pos);
			transformedPos.x *= width / 2.0f;
			transformedPos.y *= -height / 2.0f;
			transformedPos.x += width / 2.0f;
			transformedPos.y += height / 2.0f;

			//Position arrays (Get3dDistance takes a float array)
			float entPos[] = { ent->o.x, ent->o.y, ent->o.z };
			float ourPos[] = { pLocal->o.x, pLocal->o.y, pLocal->o.z };

			//3D distance for resizing the ESP
			float dist = Get3dDistance(ourPos, entPos);

			glLineWidth(1.0);

			//Make sure during team games that the teams are rendered with a green box
			int * gameMode = (int*)0x50F49C;
			if (*gameMode == 0 || *gameMode == 4 || *gameMode == 5 || *gameMode == 7 || *gameMode == 13 || *gameMode == 11 || *gameMode == 14 || *gameMode == 17 || *gameMode == 16 || *gameMode == 20 || *gameMode == 21){
				if (pLocal->team == ent->team)
					tmpColors[1] = 1.0f;

				else
					tmpColors[0] = 1.0f;
			}

			else
				tmpColors[0] = 1.0f;
			
			// Draw the healthbar
			Box(transformedPos.x - 400 / dist,
				transformedPos.y + 1850 / dist,
				(8 * ent->health) / dist,
				50 / dist,
				0, 0.7, 0);

			Box(transformedPos.x - 400 / dist,
				transformedPos.y + 1850 / dist,
				800 / dist,
				50 / dist,
				0.3, 0.3, 0.3);

			//Make our line all pretty
			glEnable(GL_LINE_SMOOTH);

			BoxLine(1,										//Line width
				transformedPos.x - 400 / dist,				//X position
				transformedPos.y - 300 / dist,				//Y position
				800 / dist, 2100 / dist,					//Box xLen, Box yLen
				tmpColors[0], tmpColors[1], tmpColors[2]);	//Colors

			glDisable(GL_LINE_SMOOTH);

			glPopMatrix();
		}
	}
}

void ESP::Box(GLfloat x, GLfloat y, GLfloat xLen, GLfloat yLen, GLfloat r, GLfloat g, GLfloat b){
	//Set our attributes
	glColor3f(r, g, b);

	//Draw our box
	glBegin(GL_QUADS);
	glVertex2f(x, y);
	glVertex2f(x + xLen, y);
	glVertex2f(x + xLen, y + yLen);
	glVertex2f(x, y + yLen);
	glEnd();
}

void ESP::BoxLine(GLfloat width, GLfloat x, GLfloat y, GLfloat xLen, GLfloat yLen, GLfloat r, GLfloat g, GLfloat b){
	//Grab the current line width to prevent clashes
	GLfloat glTemp[1];
	glGetFloatv(GL_LINE_WIDTH, glTemp);

	//Set our attributes
	glColor3f(r, g, b);
	glLineWidth(width);

	//Draw out box
	glBegin(GL_LINE_LOOP);
	glVertex2f(x, y);
	glVertex2f(x + xLen, y);
	glVertex2f(x + xLen, y + yLen);
	glVertex2f(x, y + yLen);
	glEnd();

	//Restore the line width
	glLineWidth(glTemp[0]);
}

float ESP::Get3dDistance(float * vec1, float * vec2){
	return sqrt(
		pow(double(vec2[0] - vec1[0]), 2.0) +
		pow(double(vec2[1] - vec1[1]), 2.0) +
		pow(double(vec2[2] - vec1[2]), 2.0));
}

class pBounceEnts;
class pArr;
class bEnt;

class pBounceEnts{
public:
	pArr * bounceEntArr;
	char _0x0004[4];
	__int32 entCount;
};

class pArr{
public:
	bEnt * bounceEnts[32];
};

class bEnt{
public:
	char _0x0000[4];
	float pos[3];
	char _0x0010[132];
	__int32 type;
};

void GrenadeESP::Draw(){
	playerent * pLocal = *(playerent**)(0x50F4F4);
	pBounceEnts * pbEnt = (pBounceEnts*)0x510A28;
	int * bounceEntCount = (int*)0x510A30;

	for (int i = 0; i < pbEnt->entCount; i++){
		// Assign the vMatrix
		glmatrixf * glmvpmatrix = (glmatrixf*)(0x501AE8);

		// Store the entity position in a vec object
		vec pos(pbEnt->bounceEntArr->bounceEnts[i]->pos[0], pbEnt->bounceEntArr->bounceEnts[i]->pos[1], pbEnt->bounceEntArr->bounceEnts[i]->pos[2]);

		// Check if they're off the screen or if they aren't a grenade
		if (glmvpmatrix->transformw(pos) < 0.0f || pbEnt->bounceEntArr->bounceEnts[i]->type != 1)
			continue;

		else{
			glPushMatrix();

			// Width and height of the game window
			int width = *(DWORD*)0x510C94;
			int height = *(DWORD*)0x510C98;

			// Make sure we're drawing in an area of 800x600 (Or whatever resolution the player has)
			glViewport(0, 0, width, height);
			glMatrixMode(GL_PROJECTION);
			glLoadIdentity();
			glOrtho(0, width, height, 0, -1, 1);
			glMatrixMode(GL_MODELVIEW);
			glLoadIdentity();

			// Transform the position to screen coords
			vec transformedPos = glmvpmatrix->Transform(glmvpmatrix, pos);
			transformedPos.x *= width / 2.0f;
			transformedPos.y *= -height / 2.0f;
			transformedPos.x += width / 2.0f;
			transformedPos.y += height / 2.0f;

			//Position arrays (Get3dDistance takes a float array)
			float entPos[] = { pbEnt->bounceEntArr->bounceEnts[i]->pos[0], pbEnt->bounceEntArr->bounceEnts[i]->pos[1], pbEnt->bounceEntArr->bounceEnts[i]->pos[2] };
			float ourPos[] = { pLocal->o.x, pLocal->o.y, pLocal->o.z };

			//3D distance for resizing the ESP
			float dist = Get3dDistance(ourPos, entPos);

			glLineWidth(1.0);

			glEnable(GL_LINE_SMOOTH);

			BoxLine(1,										//Line width
				transformedPos.x - 100 / dist,				//X position
				transformedPos.y - 100 / dist,				//Y position
				200 / dist, 200 / dist,					//Box xLen, Box yLen
				1.0, 0.2, 0.2);	//Colors

			glDisable(GL_LINE_SMOOTH);

			glPopMatrix();
		}
	}
}
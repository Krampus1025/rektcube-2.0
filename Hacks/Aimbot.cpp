#include <cube.h>
#include "Vec.h"
#include "aimbot.h"

bool aimbot::teamGame(){
	int * gameMode = (int*)0x50F49C;
	if (*gameMode == 0 || *gameMode == 4 || *gameMode == 5 || *gameMode == 7 || *gameMode == 13 ||
		*gameMode == 11 || *gameMode == 14 || *gameMode == 17 || *gameMode == 16 || *gameMode == 20 || *gameMode == 21)
		return true;
	return false;
}

float aimbot::toDeg(float rads){
	return (float)(rads * (180.0 / 3.14));
}

Vec3D aimbot::calcAngles(Vec3D us, Vec3D them){
	Vec3D dists;
	dists = us - them;
	float hyp = dists.magnitude();

	Vec3D result;
	result.x = toDeg(atanf(dists.y / dists.x)) + 90;
	result.y = toDeg(asinf(dists.z / hyp));

	result.y = -result.y;
	result.z = 0;

	if (dists.x >= 0)
		result.x += 180;

	return result;
}

void aimbot::activate(){
	MessageBoxA(NULL, "THIS SHOULD NEVER BE CALLED", "ERROR", MB_OK);
}

void nearCross::activate(){
	typedef playerent * (_fastcall *_GetClientById)(int cn);
	_GetClientById GetClientById = _GetClientById((DWORD)GetModuleHandle(NULL) + 0x27320);

	playerent * pLocal = *(playerent**)0x50F4F4;
	playerent * toShoot = NULL;
	float lowestDist = 999999.0f;

	// Loop through all the entities
	for (int i = 0; i < 32; i++){
		// Grab the ent by ID
		playerent * ent = GetClientById(i);

		// Make sure that ent is valid
		if (!ent || ent->health > 100 || ent->health <= 0 || ent == pLocal)
			continue;

		// Make sure that if it's a team game we don't aim at team members
		if (teamGame())
			if (pLocal->team == ent->team)
				continue;

		// Get the view angles we need to aim at
		Vec3D result = calcAngles(Vec3D(pLocal->o.x, pLocal->o.y, pLocal->o.z), Vec3D(ent->o.x, ent->o.y, ent->o.z ));
		// Get the difference between the angles we need, and our current ones for smoothing
		Vec3D diffs = result - Vec3D(pLocal->yaw, pLocal->pitch, pLocal->roll);

		// Normalize angles
		if (diffs.x > 180)
			diffs.x -= 360;

		if (diffs.x < -180)
			diffs.x += 360;

		// If the diffs are lower than current lowest
		if (diffs.magnitude() < lowestDist){
			// Set the new lowest distance to the diffs
			lowestDist = diffs.magnitude();
			// And our current aimed to entity to this one
			toShoot = ent;
		}
	}

	if (toShoot){
		// Calculate the needed angles
		Vec3D result = calcAngles(Vec3D(pLocal->o.x, pLocal->o.y, pLocal->o.z), Vec3D(toShoot->o.x, toShoot->o.y, toShoot->o.z));

		if (smoothing){
			// Get the difference between our wanted angles and our current angles
			Vec3D diffs = result - Vec3D(pLocal->yaw, pLocal->pitch, pLocal->roll);

			// Prevent 360 spins
			if (diffs.x > 180)
				diffs.x -= 360;

			if (diffs.x < -180)
				diffs.x += 360;

			// Smewth
			pLocal->yaw += diffs.x / smoothing;
			pLocal->pitch += diffs.y / smoothing;
		}

		else{
			// Snap to them without any smoothing
			pLocal->yaw = result.x;
			pLocal->pitch = result.y;
		}
	}
}

void nearPlayer::activate(){
	typedef playerent * (_fastcall *_GetClientById)(int cn);
	_GetClientById GetClientById = _GetClientById((DWORD)GetModuleHandle(NULL) + 0x27320);

	playerent * pLocal = *(playerent**)0x50F4F4;
	playerent * toShoot = NULL;
	float lowestDist = 999999.0f;

	// Loop through all of the entities
	for (int i = 0; i < 32; i++){
		// Grab the ent by ID
		playerent * ent = GetClientById(i);

		// Make sure the ent is valid
		if (!ent || ent->health > 100 || ent->health <= 0 || ent == pLocal)
			continue;

		// Make sure we aren't aiming at a teammate
		if (teamGame())
			if (pLocal->team == ent->team)
				continue;

		// Get the position difference between them and us
		Vec3D diffs = Vec3D(ent->o.x, ent->o.y, ent->o.z) - Vec3D(pLocal->o.x, pLocal->o.y, pLocal->o.z);

		// If they are closer than the current closest
		if (diffs.magnitude() < lowestDist){
			// Set the new distance
			lowestDist = diffs.magnitude();
			// Set them as the marked enemy
			toShoot = ent;
		}
	}

	if (toShoot){
		// Get the needed aim vector
		Vec3D result = calcAngles(Vec3D(pLocal->o.x, pLocal->o.y, pLocal->o.z), Vec3D(toShoot->o.x, toShoot->o.y, toShoot->o.z));

		if (smoothing){
			// Grab the difference between our current view, and the needed aim vector
			Vec3D diffs = result - Vec3D(pLocal->yaw, pLocal->pitch, pLocal->roll);

			// Normalize the angles
			if (diffs.x > 180)
				diffs.x -= 360;

			if (diffs.x < -180)
				diffs.x += 360;

			// Smewth
			pLocal->yaw += diffs.x / smoothing;
			pLocal->pitch += diffs.y / smoothing;
			pLocal->roll += diffs.z / smoothing;
		}

		else{
			// Snap to them without any smoothing
			pLocal->yaw = result.x;
			pLocal->pitch = result.y;
			pLocal->roll = result.z;
		}
	}
}
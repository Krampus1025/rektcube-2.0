#include <cube.h>
#include <gl/glut.h>
#include "Radar.h"

void Radar::Draw(){
	typedef playerent * (_fastcall *_GetClientById)(int cn);
	_GetClientById GetClientById = _GetClientById((DWORD)GetModuleHandle(NULL) + 0x27320);

	playerent * pLocal = *(playerent**)(0x50F4F4);

	// Loop through all of the entities
	for (int i = 0; i < 32; i++){
		// Get the ent by ID
		playerent * ent = (playerent*)GetClientById(i);

		// Make sure the ent is valid
		if (!ent || ent->state == CS_DEAD || ent == pLocal)
			continue;

		// Store the positions in floats as we have to change them around
		float playerX = pLocal->o.x;
		float playerY = pLocal->o.y;

		float entityX = ent->o.x;
		float entityY = ent->o.y;

		//So they aren't on top of us
		if (entityX == playerX)
			entityX += 0.0001;

		if (entityY == playerY)
			entityY += 0.0001;

		// X distance between us
		float deltaX = abs(entityX - playerX);
		// Y distance between us
		float deltaY = abs(entityY - playerY);
		// The 2D distance between us
		float dist = pow(((pow(deltaX, 2)) + (pow(deltaY, 2))), 0.5);
		// Yaw needed to aim at the desired entity
		float yawID = atan(deltaY / deltaX) * 57.295779513082323;

		// Normalize so that they are put in the correct quadrant
		if (entityX < playerX && entityY > playerY)
			yawID = 180 - yawID;

		else{
			if (entityX < playerX && entityY < playerY)
				yawID = -180 + yawID;

			else
				if (entityX > playerX && entityY < playerY)
					yawID = yawID * -1;
		}

		// The distance from our current view to the desired view
		float yawDelta = pLocal->yaw - yawID + 90;
		// Get the adjacent to find the X value on the radar
		float tempIdX = (sin(yawDelta / 57.295779513082323) * dist);
		// Get the opposite to find the Y value on the radar
		float tempIdY = (cos(yawDelta / 57.295779513082323) * dist);

		//Assign the position to the radar
		float drawX = m_center + tempIdX * m_zoom;
		float drawY = m_center + tempIdY * m_zoom;

		//If it's a team game
		int * GameMode = (int*)0x50F49C;
		if (*GameMode == 0 || *GameMode == 4 || *GameMode == 5 || *GameMode == 7 || *GameMode == 13 || *GameMode == 11 || *GameMode == 14 || *GameMode == 17 || *GameMode == 16 || *GameMode == 20 || *GameMode == 21){
			if (pLocal->team == ent->team){
				//Make sure it isn't off the radar
				if (drawX >= m_outerEdge || drawX <= m_innerEdge || drawY >= m_outerEdge || drawY <= m_innerEdge){
					if (drawX >= m_outerEdge)
						drawX = m_outerEdge;

					if (drawY >= m_outerEdge)
						drawY = m_outerEdge;

					if (drawX <= m_innerEdge)
						drawX = m_innerEdge;

					if (drawY <= m_innerEdge)
						drawY = m_innerEdge;

					//Draw the grey dot
					Dot(4, drawX, drawY, 0.8f, 0.8f, 0.8f);
				}

				else
					Dot(4, drawX, drawY, 0.0f, 1.0f, 0.0f);
			}

			//If they aren't on our team
			else if (pLocal->team != ent->team){
				//Make sure they aren't off the radar
				if (drawX >= m_outerEdge || drawX <= m_innerEdge || drawY >= m_outerEdge || drawY <= m_innerEdge){
					if (drawX >= m_outerEdge)
						drawX = m_outerEdge;

					if (drawY >= m_outerEdge)
						drawY = m_outerEdge;

					if (drawX <= m_innerEdge)
						drawX = m_innerEdge;

					if (drawY <= m_innerEdge)
						drawY = m_innerEdge;

					//Draw the grey dot
					Dot(4, drawX, drawY, 0.8f, 0.8f, 0.8f);
				}

				//Draw the red dot
				Dot(4, drawX, drawY, 1.0f, 0.0f, 0.0f);
			}
		}

		//If it isn't a team game
		else{
			//Make sure they aren't off the radar
			if (drawX >= m_outerEdge || drawX <= m_innerEdge || drawY >= m_outerEdge || drawY <= m_innerEdge){
				if (drawX >= m_outerEdge)
					drawX = m_outerEdge;

				if (drawY >= m_outerEdge)
					drawY = m_outerEdge;

				if (drawX <= m_innerEdge)
					drawX = m_innerEdge;

				if (drawY <= m_innerEdge)
					drawY = m_innerEdge;

				//Draw the grey dot
				Dot(4, drawX, drawY, 0.8f, 0.8f, 0.8f);
			}

			//Draw a red dot
			Dot(4, drawX, drawY, 1.0f, 0.0f, 0.0f);
		}
	}

	//Draw the radar cross
	Line(2, m_center, m_innerEdge, m_center, m_outerEdge, 1.0f, 1.0f, 1.0f);
	Line(2, m_innerEdge, m_center, m_outerEdge, m_center, 1.0f, 1.0f, 1.0f);

	//Draw the radar box
	Box(m_innerEdge, m_innerEdge, m_outerEdge - m_innerEdge, m_outerEdge - m_innerEdge, 0.2f, 0.2f, 0.2f);
}

void Radar::Grow(){
	m_center += 5;
	m_outerEdge += 10;
}

void Radar::Shrink(){
	m_center -= 5;
	m_outerEdge -= 10;
}

void Radar::ZoomIn(){
	if (m_zoom < 1.5)
		m_zoom += 0.1;
}

void Radar::ZoomOut(){
	if (m_zoom > 0.5)
		m_zoom -= 0.1;
}

void Radar::Dot(GLfloat size, GLfloat x, GLfloat y, GLfloat r, GLfloat g, GLfloat b){
	//Grab the current line width to prevent clashes
	GLfloat glTemp[1];
	glGetFloatv(GL_POINT_SIZE, glTemp);

	//Set our attributes
	glColor3f(r, g, b);
	glPointSize(size);

	//Draw our point
	glBegin(GL_POINTS);
	glVertex2f(x, y);
	glEnd();

	//Restore to prevent clashing
	glPointSize(glTemp[0]);
}

void Radar::Line(GLfloat width, GLfloat x1, GLfloat y1, GLfloat x2, GLfloat y2, GLfloat r, GLfloat g, GLfloat b){
	//Grab the current line width to prevent clashes
	GLfloat glTemp[1];
	glGetFloatv(GL_LINE_WIDTH, glTemp);

	//Set our attributes
	glColor3f(r, g, b);
	glLineWidth(width);

	//Draw our line
	glBegin(GL_LINES);
	glVertex2f(x1, y1);
	glVertex2f(x2, y2);
	glEnd();

	//Restore the line width
	glLineWidth(glTemp[0]);
}

void Radar::Box(GLfloat x, GLfloat y, GLfloat xLen, GLfloat yLen, GLfloat r, GLfloat g, GLfloat b){
	//Set our attributes
	glColor3f(r, g, b);

	//Draw our box
	glBegin(GL_QUADS);
	glVertex2f(x, y);
	glVertex2f(x + xLen, y);
	glVertex2f(x + xLen, y + yLen);
	glVertex2f(x, y + yLen);
	glEnd();
}

float Radar::Get2dDistance(float * vec1, float * vec2){
	return sqrt(((vec2[0] - vec1[0]) * (vec2[0] - vec1[0])) + 
				((vec2[1] - vec1[1]) * (vec2[1] - vec1[1])));
}
class Vec3D;

class aimbot{
public:
	aimbot() : smoothing(3000) { }
	aimbot(int smoothing) : smoothing(smoothing) { }

	virtual void activate() = 0;
	void setSmoothing(int newSmooth) { smoothing = newSmooth; };
	int getSmoothing() { return smoothing; };

	int curType(){
		switch (type){
		case NEAR_PLAYER:
			return 0;
			break;

		case NEAR_CROSS:
			return 1;
			break;
		}
	}

	void setType(int type){
		switch (type){
		case 0:
			type = NEAR_PLAYER;
			break;

		case 1:
			type = NEAR_CROSS;
			break;
		}
	}

protected:
	int smoothing;
	bool teamGame();
	float toDeg(float rads);
	Vec3D calcAngles(Vec3D us, Vec3D them);

	struct cmpDists{
		bool operator () (float lhs, float rhs){
			return lhs < rhs;
		}
	};

	enum aimType{
		NEAR_PLAYER,
		NEAR_CROSS
	}type;
};

class nearCross : public aimbot{
public:
	nearCross() : aimbot() { type = NEAR_CROSS; }
	nearCross(int smoothing) : aimbot(smoothing) { type = NEAR_CROSS; }

	virtual void activate();
};

class nearPlayer : public aimbot{
public:
	nearPlayer() : aimbot() { type = NEAR_PLAYER; }
	nearPlayer(int smoothing) : aimbot(smoothing) { type = NEAR_PLAYER; }

	virtual void activate();
};
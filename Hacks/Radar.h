class Radar{
public:
	Radar() : m_on(false), m_center(160), m_innerEdge(10), m_outerEdge(310), m_zoom(1) { }

	void Draw();
	bool State() { return m_on; }
	void Toggle() { m_on = !m_on; }

	void Grow();
	void Shrink();
	void ZoomIn();
	void ZoomOut();

	float getZoom() { return m_zoom; }
	int getSize() { return m_outerEdge; }

private:
	void Dot(GLfloat size, GLfloat x, GLfloat y, GLfloat r, GLfloat g, GLfloat b);
	void Line(GLfloat width, GLfloat x1, GLfloat y1, GLfloat x2, GLfloat y2, GLfloat r, GLfloat g, GLfloat b);
	void Box(GLfloat x, GLfloat y, GLfloat xLen, GLfloat yLen, GLfloat r, GLfloat g, GLfloat b);
	float Get2dDistance(float * vec1, float * vec2);

	bool m_on;
	int m_center;
	int m_innerEdge;
	int m_outerEdge;
	float m_zoom;
};
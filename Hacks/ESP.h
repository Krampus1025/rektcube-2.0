class ESP{
public:
	ESP() : m_on(false) { }

	virtual void Draw();
	bool State() { return m_on; }
	void Toggle() { m_on = !m_on; }

protected:
	void Box(GLfloat x, GLfloat y, GLfloat xLen, GLfloat yLen, GLfloat r, GLfloat g, GLfloat b);
	void BoxLine(GLfloat width, GLfloat x, GLfloat y, GLfloat xLen, GLfloat yLen, GLfloat r, GLfloat g, GLfloat b);
	float Get3dDistance(float * vec1, float * vec2);

	bool m_on;
};

class GrenadeESP : public ESP{
public:
	GrenadeESP() : ESP() { }

	virtual void Draw();
};
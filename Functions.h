#include <cube.h>
#include <gl/glut.h>
#include <conio.h>
#include "Graphics.h"
#include "Hook.h"
#include "Aimbot.h"
#include "Radar.h"
#include "ESP.h"

aimbot * aim;
Radar * radar;
ESP * esp;
GrenadeESP * gEsp;

bool Menu = false;
bool Wallhack = false;

// Draws the hack menu
void DrawMenu(){
	glPushMatrix();

	int width = *(DWORD*)0x510C94;
	int height = *(DWORD*)0x510C98;
	
	//Make sure we're drawing in an area of 800x600
	glViewport(0, 0, width, height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0, width, height, 0, -1, 1);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	//Menu outline
	Graphics draw;
	glEnable(GL_LINE_SMOOTH);
	draw.boxLine(4, width / 2 - 125, height / 2 - 175, 250, 350, 0.9f, 0.9f, 0.9f, 2);
	glDisable(GL_LINE_SMOOTH);

	//Menu
	draw.box(width / 2 - 125, height / 2 - 175, 250, 350, 0.7f, 0.7f, 0.7f);
	
	glDisable(GL_DEPTH_TEST);

	draw.string(width / 2 - 120, height / 2 - 155,	//Position
			   0.85f, 0.2f, 0.67f,					//Colors
			   GLUT_BITMAP_HELVETICA_18,			//Font
			   "Radar: %s\n"						//Text
			   "Aim to %s\n"
			   "Smoothing: %d\n"
			   "ESP: %s\n"
			   "Grenade ESP: %s\n"
			   "Radar Zoom: %f\n"
			   "Radar Size: %d\n"
			   "\n\n\n\n\n\n\n\n"
			   "Credits to Spock, GAFO666,\n"
			   "and AnomanderRake!",
			   radar->State() ? "On" : "Off",
			   aim->curType() == 0 ? "near player" : "near cross",
			   aim->getSmoothing(),
			   esp->State() ? "On" : "Off",
			   gEsp->State() ? "On" : "Off",
			   radar->getZoom(),
			   radar->getSize());

	glEnable(GL_DEPTH_TEST);
}

HMODULE hMod = GetModuleHandle("opengl32.dll");

typedef void(WINAPI * tglBegin) (GLenum mode);
typedef void(WINAPI * tglClear) (GLbitfield mask);
typedef void(WINAPI * twglSwapBuffers) (HDC hDc);

tglBegin oglBegin;
tglClear oglClear;
twglSwapBuffers owglSwapBuffers;

bool glBeginHooked = false;
bool glClearHooked = false;
bool SwapBuffHooked = false;

void WINAPI hkglBegin(GLenum mode){
	if (Wallhack)
		if (mode == GL_TRIANGLE_STRIP || mode == GL_TRIANGLES || mode == GL_TRIANGLE_FAN)
			glDisable(GL_DEPTH_TEST);

	(*oglBegin)(mode);
}

void WINAPI hkglClear(GLbitfield mask){
	if (mask == GL_DEPTH_BUFFER_BIT)
		mask = GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT;

	(*oglClear)(mask);
}

void WINAPI hkwglSwapBuffs(_In_ HDC hDc){
	if (esp->State())
		esp->Draw();

	if (gEsp->State())
		gEsp->Draw();

	if (radar->State())
		radar->Draw();

	if (Menu)
		DrawMenu();

	return owglSwapBuffers(hDc);
}
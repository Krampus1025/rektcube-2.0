#include <Windows.h>
#include <tlhelp32.h>
#include <Psapi.h>

class Hook{
protected:
	void * m_hkAddy;
	void * m_hkFnctAddy;
	BYTE * m_restoreBytes;
	DWORD m_len;
	bool m_isHooked;
public:
	Hook(void * hkAddy, void * hkFnctAddy, DWORD len);
	~Hook();

	virtual bool Restore() = 0;
	virtual void Unload() = 0;

	bool Hooked() { return m_isHooked; }
};

class trpHook : public Hook{
	virtual bool Restore();
public:
	trpHook(void * hkAddy, void * hkFnctAddy, DWORD len);
	void * ToggleHook(void * oFunct);
	void * CreateDetour();
	virtual void Unload();
};

class plainHook : public Hook{
	virtual bool Restore();
	bool CreateDetour();
	DWORD FindPattern();

	void findFirstByte(DWORD &curAddy, DWORD base, DWORD size, DWORD patLength){
		int i = 0;

		while (m_pattern[0] != *(char*)(curAddy + i)){
			if (curAddy >= base + size - patLength)
				break;

			i++;
		}
	}

	char * m_pattern;
	char * m_module;
	char * m_mask;	
public:
	plainHook(char * module, void * hkFnctAddy, char * pattern, char * mask, DWORD len);
	plainHook(void * hkAddy, void * hkFnctAddy, DWORD len);
	virtual void ToggleHook();
	virtual void Unload();
};